import * as React from "react";
import { Helmet } from "react-helmet";
import { Card } from "../../components/card/Card";
import { useTextInputQuery } from "../../hooks/useTextInputQuery";
import "./title-generator.scss";

const TITLE_TEXT_QUERY_STRING_PARAMETER_NAME = "title";
const TITLE_COLOUR_QUERY_STRING_PARAMETER_NAME = "colour";

const SUPPORTED_CHARACTERS_REGEX = /[^a-zA-Z!@#? ]+/;

const COLOURS = ["white", "yellow"];

const COLOUR_HINTS: { [colour: string]: string } = {
  white: "⚪⚪⚪⚪",
  yellow: "🟡🟡🟡🟡",
  alternate: "⚪🟡⚪🟡",
};

const INITIAL_COPY_BUTTON_TEXT = "📋 Copy";

export const TitleGenerator = () => {
  const titleText = useTextInputQuery(TITLE_TEXT_QUERY_STRING_PARAMETER_NAME);
  const colourConfiguration = useTextInputQuery(TITLE_COLOUR_QUERY_STRING_PARAMETER_NAME, "white");
  const [copyButtonText, setCopyButtonText] = React.useState(INITIAL_COPY_BUTTON_TEXT);

  const handleTitleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.target.value = e.target.value.replace(SUPPORTED_CHARACTERS_REGEX, "");

    setCopyButtonText(INITIAL_COPY_BUTTON_TEXT);

    return titleText.handleChange(e);
  };

  const output = generateSlackEmojiCode(titleText.value, colourConfiguration.value);

  const copyToClipboard = () => {
    if (!output) {
      setCopyButtonText("🙃 Write your title first");
      return;
    }

    navigator.clipboard
      .writeText(output)
      .then(() => setCopyButtonText("🎉 Done! It looks great!"))
      .catch(() => setCopyButtonText("🔥 Oh no, that didn't work"));
  };

  return (
    <section>
      <Helmet>
        <title>🔠 Title generator</title>
      </Helmet>

      <header>
        <h2>🔠 Title generator</h2>

        <p>For making eye-catching titles using Slack's letter emojis</p>
      </header>

      <div className="grid title-generator--creator">
        <Card>
          <h3>✏️ Text</h3>

          <input
            type="text"
            name="title-generator-text"
            value={titleText.value}
            onChange={handleTitleTextChange}
            placeholder="Slack Snacks!"
          />
        </Card>

        <Card>
          <h3>🎨 Configuration</h3>

          {COLOURS.concat("alternate").map((colour) => (
            <label key={colour}>
              <input
                checked={colourConfiguration.value === colour}
                onChange={colourConfiguration.handleChange}
                type="radio"
                value={colour}
                name="title-generator-colour"
              />
              {capitalise(colour)} {COLOUR_HINTS[colour]}
            </label>
          ))}
        </Card>

        <Card style={{ gridColumn: "1 / span 2" }}>
          <h3>✨ Output</h3>

          <div className="title-generator--output">
            <textarea
              value={output}
              rows={Math.ceil(titleText.value.length / 6)}
              placeholder="🐈🐈‍⬛ We're keeping this box warm while you choose your title"
              onChange={() => {}}
              onClick={(e) => {
                e.currentTarget.select();
              }}
            />

            <button role="button" onClick={copyToClipboard}>
              {copyButtonText}
            </button>
          </div>

          <h3>👀 Preview</h3>

          <em>I haven't done this yet</em>
        </Card>
      </div>
    </section>
  );
};

const capitalise = (string: string) => string.charAt(0).toUpperCase() + string.slice(1);

const specialCharacters: { [character: string]: string } = {
  "#": "hash",
  "!": "exclamation",
  "?": "question",
  "@": "at",
};

const generateSlackEmojiCode = (text: string, colour: string) =>
  text
    .split("")
    .map((character, index) => {
      if (character === " ") {
        return "  ";
      }

      const characterCode = character in specialCharacters ? specialCharacters[character] : character.toLowerCase();

      const colourCode = colour === "alternate" ? (index % 2 === 0 ? "white" : "yellow") : colour;

      // :alphabet-COLOUR-CHARACTER:
      return `:alphabet-${colourCode}-${characterCode}:`;
    })
    .join("");
