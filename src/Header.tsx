import * as React from "react";
import { Link } from "react-router-dom";

export const Header = () => (
  <header style={{ paddingBottom: 0 }}>
    <h1>
      <Link to="">🪀 Slack Toys 🧸</Link>
    </h1>

    <nav style={{ marginBottom: "0px" }}>
      <ul>
        <li>
          <Link to="title-generator">🔠 Title generator</Link>
        </li>

        <li>
          <Link to="snippet-manager">✂️ Snippet Manager (wip)</Link>
        </li>
      </ul>
    </nav>

    <hr style={{ margin: 0 }} />
  </header>
);
