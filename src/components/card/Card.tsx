import * as React from "react";
import "./card.scss";

export const Card = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>) => (
  <div {...props} className={`card${props.className ?? ""}`} />
);
