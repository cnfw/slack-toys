import { ChangeEvent } from "react";
import { useSearchParams } from "react-router-dom";

export const useTextInputQuery = (queryStringParameterName: string, defaultValue = "") => {
  const [searchParameters, setSearchParameters] = useSearchParams();
  const value = searchParameters.get(queryStringParameterName) || defaultValue;

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.value) {
      searchParameters.set(queryStringParameterName, e.target.value);
    } else {
      searchParameters.delete(queryStringParameterName);
    }

    setSearchParameters(searchParameters);
  };

  return {
    value,
    handleChange,
  };
};
