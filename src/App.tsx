import * as React from "react";
import { Helmet } from "react-helmet";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { TitleGenerator } from "./features/title-generator/TitleGenerator";
import { Header } from "./Header";

export const App = () => {
  return (
    <BrowserRouter basename={document.location.pathname}>
      <Helmet titleTemplate="%s ― 🪀 Slack Toys 🧸" defaultTitle="🪀 Slack Toys 🧸" />

      <Header />

      <main>
        <Routes>
          <Route path="/" element={<blockquote>☝️ Choose a toy to get started ☝️</blockquote>} />

          <Route path="/title-generator" element={<TitleGenerator />} />

          <Route path="/snippet-manager" element={<p>This one's not ready. Will it ever be? Who knows.</p>} />
        </Routes>
      </main>
    </BrowserRouter>
  );
};
